import json
import difflib
from difflib import SequenceMatcher
from difflib import get_close_matches

# with open('data.json') as json_file:
data = json.load(open('data.json'))


def translate(word):
    if word in data:
        return data[word]
    # elif len(get_close_matches(word, data.keys())) > 0:
     #   return "do you think one of this: " + str(get_close_matches(word, data.keys(), n=10, cutoff=0.6))
    elif len(get_close_matches(word, data.keys())) > 0:
        aversion = get_close_matches(word, data.keys())[0]
        bversion = get_close_matches(word, data.keys())[1]
        cversion = get_close_matches(word, data.keys())[2]
        return "Did you mean {}, {} or {} instead?".format(aversion, bversion, cversion)
    else:
        return "theres no such word"


word = input("Enter word: ").lower()
while word != 'quitquit':
    output = translate(word)
    if type(output) == list:
        for item in output:
            print('1', item)
    else:
        print('2', output)
    word = input("Enter word: ").lower()
    print('Type quitquit if you want to exit')
