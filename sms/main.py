import requests
import json
from twilio.rest import Client
from twilio.http.http_client import TwilioHttpClient
import os

account_sid = 'ACbd9813b45569690fde904d382b0a9f20'
auth_token = '63ee1595e2244ec98b6569231a7ffd00'
api_key = '41f01e5d582a321b17a62d3c6649e196'
endpoint = 'https://api.openweathermap.org/data/2.5/onecall'
weather_parameters = {
    "lat": 47.786709,
    "lon": 18.958670,
    "appid": api_key,
    "exclude": 'current, minutely, daily',
}

response = requests.get(endpoint, params=weather_parameters)
js = response.json()

print(response.status_code)
print(js)

will_rain = False

twelve_hours = js['hourly'][:12]
for time in twelve_hours:
    for day in time['weather']:
        if int(day['id']) < 700:
            will_rain = True

if will_rain:
    proxy_client = TwilioHttpClient()
    proxy_client.session.proxies = {'https': os.environ['https_proxy']}
    client = Client(account_sid, auth_token, proxy_client)
    message = client.messages \
        .create(
        body="Ma esni fog, ne felejts magaddal esernyőt hozni",
        from_='+15736853430',
        to='+***********'
    )
else:
    proxy_client = TwilioHttpClient()
    proxy_client.session.proxies = {'https': os.environ['https_proxy']}
    client = Client(account_sid, auth_token, proxy_client)
    message = client.messages \
        .create(
        body="Napos idő lesz",
        from_='+15736853430',
        to='+***********')

print(message.status)