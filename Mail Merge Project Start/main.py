#TODO: Create a letter using starting_letter.txt 
#for each name in invited_names.txt
#Replace the [name] placeholder with the actual name.
#Save the letters in the folder "ReadyToSend".
    
#Hint1: This method will help you: https://www.w3schools.com/python/ref_file_readlines.asp
    #Hint2: This method will also help you: https://www.w3schools.com/python/ref_string_replace.asp
        #Hint3: THis method will help you: https://www.w3schools.com/python/ref_string_strip.asp

with open('./Input/Names/invited_names.txt') as names_file:
    names_data = names_file.read().splitlines()
    names_list = []
    for name in names_data:
        names_list.append(name)
    print(names_list)

with open('./Input/Letters/starting_letter.txt') as letter_file:
    letter_data = letter_file.read()

for name in names_list:
    with open(f'./Output/ReadyToSend/{name}.txt', mode='a') as output_file:
        output_file.write(letter_data.replace("[name]", name))


