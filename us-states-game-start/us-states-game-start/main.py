import turtle
from turtle import Turtle, Screen
import pandas

screen = Screen()
screen.title("USA States Game")
img = "blank_states_img.gif"
screen.addshape(img)

turtle.shape(img)

#
# def get_mouse_click_coordinate(x, y):
#     print(x, y)
#
#
# turtle.onscreenclick(get_mouse_click_coordinate)
#
# turtle.mainloop()
Good = True
data = pandas.read_csv("50_states.csv")
score = 0
good_answeres = []
while Good:
    answer_state = screen.textinput(title="Guess a state", prompt="What's another state name?")

    answer = data[data.state == answer_state]



    state = Turtle()

    state.pu()
    try:
        state.goto(float(answer['x']), float(answer['y']))
        state.hideturtle()
        state.write(answer)

        if answer not in good_answeres:
            good_answeres.append(answer)
            score += 1

    except:
        print('game over ', score)
        score = 0
        Good = False

screen.exitonclick()

# Found = False
# state_index = 0
# print(len(data['state']))
#
# while state_index <= len(data['state']) and not Found:
#     state_index += 1
#     if data['state'][state_index] == answer_state:
#         print(state_index)
#         Found = True
