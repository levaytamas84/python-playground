
table = [" "] * 9
last = "X"

def print_table():
    print()
    print("|", table[0],"|",table[1],"|",table[2],"|")
    print("|", table[3],"|",table[4],"|",table[5],"|")
    print("|", table[6],"|",table[7],"|",table[8],"|")
    print()
    

print_table()


def mark(icon):
    
    mark = int(input('add a number: '))
    while table[mark-1] != " ":
        print('that spot is occupied')
        mark = int(input('add a different number: '))
    table[mark-1] = icon
    print_table()
    global last
    last = icon

def X_turn(icon):
    if icon == "X":
        return True
    else:
        return False

def is_won(icon):
    if table[0] == icon and table[1] == icon and table[2] == icon or\
    table[3] == icon and table[4]== icon and table[5] ==icon or\
    table[6]== icon and table[7]== icon and table[8] == icon or\
    table[0]== icon and table[3]== icon and table[6] == icon or\
    table[1]== icon and table[4]== icon and table[7] == icon or\
    table[2]== icon and table[5]== icon and table[8] == icon or\
    table[0]== icon and table[4]== icon and table[8] == icon or\
    table[2]== icon and table[4]== icon and table[7] == icon:
        string ="{} won".format(icon)
        print(string)
        return True
    else:
        return False

def is_draw():
    if " " not in table:
        return True
    else:
        return False

while True:
    if last == "X":
        mark("X")
        if(is_won("X")):
            break
    else:
        mark("O")
        if(is_won("O")):
            break
    if last =="X":
        last = "O"
    else:
        last = "X"
    if(is_draw()):
       print("DRAW")
       break


print(last)





    
