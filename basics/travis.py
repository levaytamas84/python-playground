known_users=['Alice','Bob','Claire','Dan','Emma','Fred','Georgie','Harry']
print(len(known_users))
while True:
    print('Hi! My name is Travis')
    name = input('What is your name: ').strip().capitalize()
    if(name not in known_users):
        known_users.append(name)
        print("Hello {}!".format(name))
    else:
        print("Hello " + name)
    print(known_users)
