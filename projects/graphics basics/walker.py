from turtle import Turtle, Screen
import random

screen = Screen()
screen.colormode(255)
t = Turtle()
t.shape('arrow')
t.pensize(10)
t.speed(10)

def random_color():
    r = random.randint(0,255)
    g = random.randint(0,255)
    b = random.randint(0,255)
    color = (r,g,b)
    return color


def random_pipe(corner_angle):
    rand_a = random.randint(0,255)
    rand_b = random.randint(0,255)
    rand_c = random.randint(0,255)
    #angles = [0,90,180,270]
    angles =[angle for angle in range(360) if angle %corner_angle == 0]
    randomangle = random.choice(angles)
    
    #t.color(rand_a,rand_b,rand_c)
    t.color(random_color())
    t.forward(20)
    #t.right(randomangle)
    t.setheading(randomangle)

for i in range(1,1000):
    random_pipe(90)
    print(i)
screen.exitonclick()

