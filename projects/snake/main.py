from turtle import Screen
from snake import Snake
from food import Food
from score import Score
import time

screen = Screen()
screen.screensize(canvwidth=600, canvheight=600)
screen.bgcolor("black")
screen.title("Python - Snake")
screen.tracer(0)

snake = Snake()
food = Food()
score = Score()
screen.listen()
screen.onkey(snake.up, "Up")
screen.onkey(snake.down, "Down")
screen.onkey(snake.left, "Left")
screen.onkey(snake.right, "Right")

game_on: bool = True
while game_on:
    screen.update()
    time.sleep(0.1)
    snake.move()

    if snake.head.distance(food) < 15:
        food.refresh()
        snake.grow_snake()
        score.upgrade_points()
    if snake.collision_wall():
        score.game_over()
        game_on = False
    if snake.collision_snake():
        score.game_over()
        game_on=False

screen.exitonclick()
