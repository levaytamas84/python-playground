from turtle import Turtle


class Score(Turtle):

    def __init__(self):
        super(Score, self).__init__()
        self.hideturtle()
        self.color("white")
        self.pu()
        self.goto(0, 380)
        self.points = 0
        self.write("Score ={}".format(self.points), False, align="center")

    def upgrade_points(self):
        self.points += 1
        self.clear()
        self.write("Score ={}".format(self.points), False, align="center")

    def game_over(self):
        self.pu()
        self.goto(0, 0)
        self.write("GAME OVER", align="center", font=("Arial", 14, "normal"))
