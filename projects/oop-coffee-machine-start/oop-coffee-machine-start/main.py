from menu import Menu, MenuItem
from coffee_maker import CoffeeMaker
from money_machine import MoneyMachine

coffeemaker = CoffeeMaker()
moneymachine = MoneyMachine()
menu = Menu()


machineOn = True

while machineOn:
    coffeemaker.report()
    moneymachine.report()
    theMenu = menu.get_items()
    print(theMenu)
    options = input('Choose drink or type OFF to exit: ').lower()
    if options == 'off':
        machineOn = False
        break
    drink = menu.find_drink(options)
    if moneymachine.make_payment(drink.cost):
        coffeemaker.make_coffee(drink)
    else:
        print('Sorry no money')







