class QuizBrain:
    question_number: int

    def __init__(self, question_list):
        self.question_number = 0
        self.question_list = question_list
        self.point = 0

    def is_there_more_questions(self):
        if self.question_number < len(self.question_list):
            return True
        else:
            return False

    def next_question(self):
        current_question = self.question_list[self.question_number]

        answer = input(f"Q.{self.question_number} :{current_question.text} (True/False): ")
        if answer == self.question_list[self.question_number].answer:
            print("yeah")
            self.point += 1
            print('You got it right!')
            print(f'The correct answer was: {answer}')
            print(f'Your current score is: {self.point}/{self.point}')
            print('SQN11', self.question_number)
            self.question_number += 1
            if self.is_there_more_questions():
                self.next_question()

        else:
            print('You was wrong')
            print(f'The correct answer was not: {answer}')
            print(f'Your final score was: {self.point + 1} / {self.point}')
            return
        print('SQN 22 ', self.question_number)
