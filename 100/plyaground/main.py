def add(*args):

    summa = 0
    for arg in args:
        summa += arg
    return summa


#print(add(2, 3, 4, 5))


def calculate(n, **kwargs):

    # for key, value in kwargs.items():
    #     print(key,value)

    # print('added with 3', n + kwargs["add"])
    # print(f'multiply with {kwargs["multiply"]}', n * kwargs["multiply"])

    n += kwargs["add"]
    n *= kwargs["multiply"]
    return n

print((calculate(2, add=3, multiply=5)))

class Car:
    def __init__(self, **kw):
        self.make = kw["make"]
        self.model = kw.get("model")
        self.color = kw.get("color")

my_car = Car(make="Nissan", model="Vanette Cargo")
print(my_car.make)
print(my_car.model)
print(my_car.color)