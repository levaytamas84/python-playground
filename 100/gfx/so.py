import random
import turtle as t

screen = t.Screen()
screen.colormode(255)
color_list = [(190, 163, 120), (223, 217, 103), (124, 169, 196), (148, 86, 52), (41, 110, 157), (161, 165, 40),
              (133, 180, 144), (150, 16, 34), (192, 142, 152), (45, 31, 20), (149, 67, 84), (14, 36, 67), (56, 121, 72),
              (80, 16, 26), (187, 89, 108), (78, 163, 98), (197, 94, 71), (21, 48, 30), (46, 158, 191), (225, 221, 15),
              (15, 58, 126), (18, 98, 48), (137, 31, 23), (217, 176, 183), (170, 206, 177), (216, 179, 173),
              (71, 74, 29), (93, 127, 171), (176, 190, 213), (162, 203, 213), (22, 84, 99)]

t.speed('fastest')
t.penup()
t.hideturtle()
t.setheading(225)
t.forward(300)
t.setheading(0)
number_of_dots = 100

for dot in range(1, number_of_dots +1):
    t.dot(20, random.choice(color_list))
    t.forward(50)

    if dot % 10 == 0:
        t.setheading(90)
        t.forward(50)
        t.setheading(180)
        t.forward(500)
        t.setheading(0)




screen.exitonclick()
