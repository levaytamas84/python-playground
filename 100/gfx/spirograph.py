from turtle import Turtle, Screen
import random

screen = Screen()
screen.colormode(255)
t = Turtle()
t.speed(0)

def random_color():
    r = random.randint(0,255)
    g = random.randint(0,255)
    b = random.randint(0,255)
    color = (r,g,b)
    return color


def spirograph(gap_size):
    for i in range(int(360/gap_size)):
        t.color(random_color())
        #t.tilt(30)
        t.right(5)
        #t.setheading(t.heading()+5)
    #t.left(20)
        t.circle(100)

spirograph(10)
screen.exitonclick()