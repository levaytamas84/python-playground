import pandas
import csv

data = pandas.read_csv('weather_data.csv')

temps = []
data_dict = data.to_dict()
data_list = data["temp"].to_list()

for temp in data["temp"]:
    temps.append(temp)

# print(temps)
# print(data_dict)
# print(data_list)

avg = data["temp"].mean()

# print(avg)

max = data["temp"].max()

print(data[data.temp == max])


def average_temp(lista):
    summa = 0
    for elem in lista:
        summa += elem
    average = summa / len(lista)
    return average

def fahrenheit(x):
    x = x * 1.8 + 32
    return float(x)


for temp in temps:
    print(fahrenheit(temp))

# print(average_temp(data_list))

# with open("weather_data.csv") as data_file:
#     datas = csv.reader(data_file)
#     temperatures = []
#     for row in datas:
#         print(row[1])
#         if row[1] != 'temp':
#             temperatures.append(int(row[1]))
#
#     print(temperatures)
