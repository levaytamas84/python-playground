from turtle import Turtle, Screen
from paddle import Paddle
from ball import Ball
from scoreboard import Scoreboard
import time
import tkinter as tk



root = tk.Tk()

#SCREEN_WIDTH = root.winfo_screenwidth()
#SCREEN_HEIGHT = root.winfo_screenheight()

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
root.destroy()
print("SW",SCREEN_WIDTH)
print("Sh", SCREEN_HEIGHT)

screen = Screen()
screen.bgcolor("black")
screen.setup(width=SCREEN_WIDTH, height=SCREEN_HEIGHT)
screen.title("Pong")
screen.tracer(0)

#right_paddle = Paddle((SCREEN_WIDTH /2, 0))
#left_paddle = Paddle((-SCREEN_WIDTH *2, 0))

right_paddle = Paddle((350, 0))
left_paddle = Paddle((-350, 0))

screen.listen()
screen.onkey(right_paddle.go_up, "Up")
screen.onkey(right_paddle.go_down, "Down")
screen.onkey(left_paddle.go_up, "w")
screen.onkey(left_paddle.go_down, "s")

ball = Ball()
score = Scoreboard()
game_is_on = True
dir_x = 10
dir_y = 10

#score_left = 0
#score_right = 0
#score = Turtle()
#score.hideturtle()
#score.color("white")
#score.penup()
#score.goto(x=0, y=300)
#score.write(f'{score_left} : {score_right}', align="center", font=("Comic Sans MS", 14, "normal"))

while game_is_on:
    time.sleep(0.1)
    screen.update()
    ball.move()
    print(ball.xcor())
    #if ball.ycor() > (SCREEN_HEIGHT / 2 - ball.width() * 6) or ball.ycor() < (-SCREEN_HEIGHT /2 + ball.width() *6):
    if ball.ycor() > 280 or ball.ycor() < -280:
        ball.bounce_y()
    #detect collision with right paddle
    if ball.distance(right_paddle) < 50 and ball.xcor() > 320:
        ball.bounce_x()
        #ball.bounce_y()
    if ball.distance(left_paddle) < 50 and ball.xcor() < -320:
        ball.bounce_x()
        #ball.bounce_y()
    if ball.xcor() < -380:
        score.r_point()
        ball.reset_position()
        ball.speedup()
    if ball.xcor() > 380:
        score.l_point()
        ball.reset_position()
        ball.speedup()



screen.exitonclick()
