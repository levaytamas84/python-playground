import pandas

data = pandas.read_csv("2018_Central_Park_Squirrel_Census_-_Squirrel_Data.csv")

#data_dict = data.to_dict()

black_squirrels = data[data["Primary Fur Color"] == "Black"]
short_blacks = black_squirrels[["Primary Fur Color", "Lat/Long"]]
cinnamon_squirrels = data[data["Primary Fur Color"] == "Cinnamon"]
gray_squirrels = data[data["Primary Fur Color"] == "Gray"]

black_squirrels_count = len(black_squirrels)
red_squirrels_count = len(cinnamon_squirrels)
gray_squirrels_count = len(gray_squirrels)

# print(len(gray_squirrels))
#
print(data.groupby("Primary Fur Color")["Primary Fur Color"].count())

data_dict = {"Fur Color": ["Gray", "Cinnamon", "Black"],
             "Count": [gray_squirrels_count, red_squirrels_count, black_squirrels_count]
             }

df = pandas.DataFrame(data_dict)
df.to_csv("squirrels_count.csv")

