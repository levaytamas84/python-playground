import requests
from datetime import datetime
from requests.auth import HTTPBasicAuth
import os



# API_ID = "1690031e"
# API_KEY = "5807e45e76c113e408222f292033c5fa"

API_ID = os.environ["EX_APP_ID"]
API_KEY = os.environ["EX_API_KEY"]

GENDER = 'male'
WEIGHT_KG = 85
HEIGHT_CM = 178
AGE = 37


EXERCISE_ENPOINT = "https://trackapi.nutritionix.com/v2/natural/exercise"

exercise_text = input("add your exercise params: ")

exercise_headers= {
    "x-app-id": API_ID,
    "x-app-key": API_KEY,
}

params = {
 "query": exercise_text,
 "gender": GENDER,
 "weight_kg": WEIGHT_KG,
 "height_cm": HEIGHT_CM,
 "age": AGE,
}
print(type(GENDER))

response = requests.post(EXERCISE_ENPOINT, json=params, headers=exercise_headers)
result = response.json()
print(result)

today_date = datetime.now().strftime("%d/%m/%Y")
now_time = datetime.now().strftime("%X")

for exercise in result["exercises"]:
    sheet_inputs = {
        "workout": {
            "date": today_date,
            "time": now_time,
            "exercise": exercise["name"].title(),
            "duration": exercise["duration_min"],
            "calories": exercise["nf_calories"],
        }
    }

SHEETY_API = "https://api.sheety.co/67c40a10f664ffc623c6851cb48db9b4/workoutTracking/workouts"

#Basic Authentication

# sheety_rp = requests.post(SHEETY_API, json=sheet_inputs, auth= HTTPBasicAuth(API_ID, API_KEY))
#
# print(sheety_rp.text)

#Bearer Authentication



headers = {"Authorization": "Bearer tomas"}

exercise_response = requests.post(SHEETY_API, json=sheet_inputs, headers=headers)

print(exercise_response.text)


