from turtle import Turtle, Screen
import random
# timmy = Turtle(shape='turtle')
# tommy = Turtle(shape='turtle')
screen = Screen()
screen.setup(width=500, height=400)
colors = ["red", "orange", "yellow", "green", "blue", "purple"]
bet = screen.textinput(title="Make a bet", prompt="Which turtle will win the race? Enter a color: ")
all_turtles = []
# timmy.color('green')
# timmy.penup()
# timmy.goto(x=-200, y=50)
# timmy.pendown()
# tommy.color('red')
# tommy.penup()
# tommy.goto(x=-200, y=100)
# tommy.pendown()

color_index =0
turtles = 0
# for i in range(-100, 200):
#     if i % 30 == 0 and turtles < 6:
#         tur = Turtle(shape='turtle')
#         tur.penup()
#         tur.goto(x=-200, y=i)
#         tur.pendown()
#         tur.color(colors[color_index])
#         turtles += 1
#         if color_index < len(colors) -1:
#             color_index += 1

# def move_turtle(turtle):
#     turtle.penup()
#     random_speed = random(1, 10)
#     turtle.forward(random_speed)



y_positions = [-70, -40, -10, 20, 50, 80]
for turtle_index in range(6):
    new_turtle = Turtle(shape='turtle')
    new_turtle.penup()
    new_turtle.goto(x=-200, y=y_positions[turtle_index])
    new_turtle.pendown()
    new_turtle.color(colors[color_index])
    turtles += 1
    color_index += 1
    all_turtles.append(new_turtle)

if bet:
    is_race_on = True

while is_race_on:

    for turtle in all_turtles:
        if turtle.xcor() > 230:
            winning_color = turtle.pencolor()
            if winning_color == bet:
                print(f'You win! The {winning_color} turtle is the winner!')
            else:
                print(f'You have lost! The {winning_color} turtle is the winner')
            is_race_on = False

        random_distance = random.randint(0, 10)
        turtle.penup()
        turtle.forward(random_distance)
        turtle.pendown()







screen.exitonclick()