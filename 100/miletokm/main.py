from tkinter import Tk, Entry, Label, Button

FONT = ("Ariel", 10)

window = Tk()
window.title('Mile to Km Converter')
window.minsize(height=100, width=200)

#empty space grid 0,0

#inputfield grid 0,1

input_field = Entry(width=10)
inputtext = input_field.get()
input_field.grid(row=0, column=1)




#text grid 0,2 Miles

miles_label = Label(text="Miles", font=FONT)
miles_label.grid(row=0, column=2)
miles_label.config(padx=10, pady=10)
#text grid 1,0 is equal

iseq_label = Label(text="is equal", font=FONT)
iseq_label.grid(row=1, column=0)

#text field what calculated grid 1,1

calculated = Label(text="0", font=FONT)
calculated.grid(row=1, column=1)

#text field Km grid 1,2

km_label = Label(text="Km", font=FONT)
km_label.grid(row=1, column=2)

#empty field 2,0


#Button Calculate grid 2,1

def calculate_km():
    mile = float(input_field.get())
    km = mile * 1.609344
    km_label.config(text=km)


calculate_button = Button(text="Calculate", font=FONT, command=calculate_km)
calculate_button.grid(row=2, column=1)
calculate_button.config(padx=10, pady=1)



#empty field 2,2



window.mainloop()