from turtle import Turtle

with open("data.txt", mode="r") as old_high_score:
    score = int(old_high_score.read())


def save_score(new_score):
    with open("data.txt", mode="w") as new_high_score:
        new_high_score.write(str(new_score))


class Score(Turtle):

    def __init__(self):
        super(Score, self).__init__()
        self.points = 0
        self.high_score = score
        self.color("white")
        self.pu()
        self.goto(0, 270)
        self.hideturtle()
        self.update_points()

    def update_points(self):
        self.clear()
        self.write("Score: {}  High-Score : {}".format(self.points, self.high_score), align="center", font = ("Courier", 24, "normal"))

    def increase_points(self):
        self.points += 1
        self.update_points()

    def reset(self):
        if self.points > self.high_score:
            self.high_score = self.points
            save_score(self.high_score)
        self.points = 0
        self.update_points()


#    def game_over(self):
#        self.pu()
#        self.goto(0, 0)
#        self.write("GAME OVER", align="center", font=("Arial", 14, "normal"))
