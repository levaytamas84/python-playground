from turtle import Turtle

STARTING_POSITIONS = [(0, 0), (-20, 0), (-40, 0)]
MOVE_DISTANCE = 20
UP = 90
DOWN = 270
RIGHT = 0
LEFT = 180
WALL_POSX = 620
WALL_NEGX = -620
WALL_POSY = 320
WALL_NEGY = -320


class Snake(Turtle):

    def __init__(self):
        self.segments = []
        self.create_snake()
        self.head = self.segments[0]

    def create_snake(self):
        for position in STARTING_POSITIONS:
            self.add_snake_segment(position)

    def add_snake_segment(self, position):
        snake_part = Turtle("square")
        snake_part.color("white")
        snake_part.pu()
        snake_part.goto(position)
        self.segments.append(snake_part)

    def grow_snake(self):
        self.add_snake_segment(self.segments[-1].position())

    def move(self):
        for seg_num in range(len(self.segments) - 1, 0, -1):
            new_x = self.segments[seg_num - 1].xcor()
            new_y = self.segments[seg_num - 1].ycor()
            self.segments[seg_num].goto(new_x, new_y)
        self.head.forward(MOVE_DISTANCE)

    def up(self):
        if self.head.heading() == LEFT or self.head.heading() == RIGHT:
            self.head.setheading(UP)
        else:
            pass

    def down(self):
        if self.head.heading() == LEFT or self.head.heading() == RIGHT:
            self.head.setheading(DOWN)
        else:
            pass

    def right(self):
        if self.head.heading() == UP or self.head.heading() == DOWN:
            self.head.setheading(RIGHT)

    def left(self):
        if self.head.heading() == UP or self.head.heading() == DOWN:
            self.head.setheading(LEFT)

    def on_food(self, food):
        return self.head.pos() == food.pos()

    def eat(self):
        snake_part = Turtle("square")
        snake_part.pu()
        snake_part.color("blue")
        self.segments.append(snake_part)
        snake_part.color("white")

    def position(self):
        return self.head.pos()

    def collision_wall(self):
        return self.head.xcor() > WALL_POSX or self.head.xcor() < WALL_NEGX \
            or self.head.ycor() > WALL_POSY or self.head.ycor() < WALL_NEGY

    def collision_snake(self):
        broken_snake = False
        for seg in self.segments[1:]:
            if self.head.distance(seg) < 10:
                broken_snake = True
            else:
                broken_snake = False
        return broken_snake

    def reset(self):
        for seg in self.segments:
            seg.goto(1000, 1000)
        self.segments.clear()
        self.create_snake()
        self.head = self.segments[0]
