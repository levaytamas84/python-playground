import time
from turtle import Screen

import car_manager
from player import Player
from car_manager import CarManager
from scoreboard import Scoreboard

import random

screen = Screen()
screen.setup(width=600, height=600)
screen.tracer(0)
screen.listen()

player = Player()
screen.onkey(player.move_forward, "Up")

car_manager = CarManager()
cars = []

scoreboard = Scoreboard()

def make_car_wave():
    for i in range(1):
        new_car = car_manager.make_car()
        cars.append(new_car)


print(player.position())
game_is_on = True
while game_is_on and player.ycor() < 300:
    time.sleep(0.1)
    screen.update()

    car_manager.make_car()
    car_manager.move_car()

    for car in car_manager.all_cars:
        if car.distance(player) < 20:
            game_is_on = False
            scoreboard.game_over()
            screen.exitonclick()
    if player.crossed_finish():
        player.go_to_start()
        car_manager.level_up()
        scoreboard.increase_level()
    #Levelup if crossing were succesful


    # if len(cars) > 0 and len(cars) < 10:
    #     make_car_wave()
    # else:
    #     for car in cars:
    #         car.clear()
    #     make_car_wave()
    # if len(cars) > 0 and len(cars) < 10:
    #     for car in cars:
    #         car.move_car()
    # for car in cars:
    #     if car.distance(player.pos()) < 20:
    #         print("GAMEOVER")
    #         game_is_on = False
    # if player.ycor() > 280:
    #     screen.clear()
    #     make_car_wave()