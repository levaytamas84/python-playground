from turtle import Turtle
import random

COLORS = ["red", "orange", "yellow", "green", "blue", "purple"]
STARTING_MOVE_DISTANCE = 5
MOVE_INCREMENT = 10


def random_pos():
    number = random.randint(-280, 250)
    return number


def random_color():
    color = random.choice(COLORS)
    return color


class CarManager(Turtle):
    def __init__(self):
        super().__init__()
        self.all_cars = []
        self.car_speed = STARTING_MOVE_DISTANCE
        self.hideturtle()

    def make_car(self):
        number = random.randint(1, 7)
        if number == 1:
            car = Turtle("square")
            car.shapesize(1, 2)
            car.penup()
            car.color(random_color())
            car.left(180)
            car.goto(300, random_pos() + 30)
            self.all_cars.append(car)

    def move_car(self):
        for car in self.all_cars:
            car.forward(self.car_speed)

    def level_up(self):
        self.car_speed += MOVE_INCREMENT