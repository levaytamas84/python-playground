from tkinter import *
import time
import math

# ---------------------------- CONSTANTS ------------------------------- #
PINK = "#e2979c"
RED = "#e7305b"
GREEN = "#9bdeac"
YELLOW = "#f7f5dd"
FONT_NAME = "Courier"
WORK_MIN = 25
SHORT_BREAK_MIN = 5
LONG_BREAK_MIN = 20
REPS = 0
TIMER = None
# ---------------------------- TIMER RESET ------------------------------- #

def reset_timer():
    window.after_cancel(TIMER)
    canvas.itemconfig(timer_text, text="0:00")
    title_label.config(text="Timer")
    mark_label.config(text="")
    global REPS
    REPS = 0


# ---------------------------- TIMER MECHANISM ------------------------------- #

def start_timer():
    global REPS
    REPS += 1
    work_sec = WORK_MIN * 60
    short_break_sec = SHORT_BREAK_MIN * 60
    long_break_sec = LONG_BREAK_MIN * 60
    if REPS % 8 == 0:
        count_down(long_break_sec)
        title_label.config(text="BREAK", fg=GREEN)
    elif REPS % 2 == 0:
        count_down(short_break_sec)
        title_label.config(text="BREAK", fg=PINK)
    else:
        count_down(work_sec)
        title_label.config(text="WORK", fg=RED)


# ---------------------------- COUNTDOWN MECHANISM ------------------------------- #


def count_down(count):
    global REPS
    count_min = math.floor(count / 60)
    count_sec = count % 60
    if count_sec < 10:
        count_sec = "0" + str(count % 60)

    canvas.itemconfig(timer_text, text=f"{count_min}:{count_sec}")
    if count > 0:
        global TIMER
        TIMER = window.after(1000, count_down, count - 1)
    else:
        start_timer()
        marks = ""

        work_sessions = math.floor(REPS/2)
        for session in range(work_sessions):
            marks += "✓"

            mark_label.config(text = marks)


        # ---------------------------- UI SETUP ------------------------------- #

window = Tk()
window.title("Pomodoro")
# window.geometry('300x300')
window.config(padx=100, pady=50, bg=YELLOW)

title_label = Label(text="TIMER", font=(FONT_NAME, 22, "bold"), fg=GREEN, bg=YELLOW)
title_label.grid(row=0, column=1)
start_button = Button(text='start', command=start_timer)
start_button.grid(row=2, column=0)
reset_button = Button(text='reset', command=reset_timer)
reset_button.grid(row=2, column=2)
mark_label = Label(font=(FONT_NAME, 22, "bold"), fg=GREEN, bg=YELLOW)
mark_label.grid(row=3, column=1)

canvas = Canvas(width=200, height=224, bg=YELLOW, highlightthickness=0)
background = PhotoImage(file='tomato.png')
canvas.create_image(100, 112, image=background)
timer_text = canvas.create_text(100, 132, text='00:00', font=(FONT_NAME, 22, "bold"), fill='white')
canvas.grid(row=1, column=1)

# window.geometry('200x240')
# background = PhotoImage(file='tomato.png')
# background_label = Label(window, image=background)
# background_label.place(x=0, y=0)
# time_counter = 0
# timer_label = Label(text=time_counter)
# timer_label.pack()
#
# start_timer = time.time()
# print(time.asctime(time.localtime(time.time())))

# timer_on = True
# while timer_on:
#     time.sleep(1)
#     time_counter += 1
#     timer_label = Label(text=time_counter)
#     timer_label.pack()


window.mainloop()
